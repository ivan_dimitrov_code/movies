package com.puzzlegramma.movie.screens.movie

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import com.puzzlegramma.movie.screens.movie.data.MovieRepository
import com.puzzlegramma.movie.screens.movie.model.app.MovieInfo
import com.puzzlegramma.movie.screens.movie.ui.MovieState
import io.reactivex.Single
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.koin.core.context.stopKoin
import org.koin.test.KoinTest
import org.mockito.ArgumentCaptor
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class MovieViewModelTest : KoinTest {
    @Mock
    private lateinit var repository: MovieRepository

    private val observer: Observer<MovieState> = mock()

    @Rule
    @JvmField
    val rule = InstantTaskExecutorRule()

    @Before
    fun before() {
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }
        MockitoAnnotations.initMocks(this)
    }

    @After
    fun after() {
        stopKoin()
    }

    @Test
    fun requestMovieData() {
        val movieList = getMovieList()
        whenever(repository.provideMovieData()).thenReturn(Single.just(movieList))
        val viewModel = MovieViewModel(repository)
        viewModel.uiState.observeForever(observer)
        viewModel.requestMovieData()
        val argumentCaptor = ArgumentCaptor.forClass(MovieState::class.java)

        argumentCaptor.run {
            verify(observer, Mockito.times(2)).onChanged(capture())
            val (loadingState, defaultState) = allValues
            assertEquals(MovieState.LoadingState, loadingState)
            assertEquals(MovieState.DefaultState(movieList), defaultState)
        }
    }

    @Test
    fun requestMovieDataError() {
        whenever(repository.provideMovieData()).thenReturn(Single.error(Throwable("errorMessage")))
        val viewModel = MovieViewModel(repository)
        viewModel.uiState.observeForever(observer)
        viewModel.requestMovieData()
        val argumentCaptor = ArgumentCaptor.forClass(MovieState::class.java)

        argumentCaptor.run {
            verify(observer, Mockito.times(2)).onChanged(capture())
            val (loadingState, defaultState) = allValues
            assertEquals(MovieState.LoadingState, loadingState)
            assertEquals(MovieState.ErrorState("errorMessage"), defaultState)
        }
    }

    fun getMovieList(): List<MovieInfo> {
        return listOf(
            MovieInfo(
                "1",
                "title",
                "1111",
                listOf("ganre"),
                listOf(1, 2, 3),
                "poster",
                "1",
                "123",
                "1999",
                4.6,
                "original title",
                "some description",
                listOf("actor 1", "actor 2"),
                "1",
                "1",
            )
        )
    }
}