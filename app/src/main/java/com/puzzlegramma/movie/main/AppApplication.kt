package com.puzzlegramma.movie.main


import android.app.Application
import com.puzzlegramma.movie.screens.movie.di.*
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class AppApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        setupKoin()
    }

    private fun setupKoin() {
        startKoin {
            androidContext(this@AppApplication)
            modules(
                listOf(
                    movieModule
                )
            )
        }
    }
}