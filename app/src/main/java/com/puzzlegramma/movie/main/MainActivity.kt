package com.puzzlegramma.movie.main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.puzzlegramma.movie.R
import com.puzzlegramma.movie.screens.details.DetailsFragment
import com.puzzlegramma.movie.screens.error.ErrorFragment
import com.puzzlegramma.movie.screens.movie.model.app.MovieInfo
import com.puzzlegramma.movie.screens.movie.ui.MovieFragment
import com.puzzlegramma.movie.utils.NetworkState

const val LAST_FRAGMENT_NAME_KEY = "LAST_FRAGMENT_NAME_KEY"

class MainActivity : AppCompatActivity(), ErrorFragment.OneErrorFragmentInteractionListener,
    MovieFragment.OnFragmentInteractionListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (savedInstanceState == null) {
            if (NetworkState.isConnectedToNetwork(this)) {
                showMovieScreen()
            } else {
                showErrorScreen()
            }
        }
    }

    private fun showFragmentWithTag(tag: String, bundle: Bundle? = null) {
        val fragment = provideFragmentForTag(tag, bundle)
        val transaction = supportFragmentManager.beginTransaction()
        fragment?.let { transaction.replace(R.id.mainFragmentContainer, it, tag) }
        transaction.addToBackStack(null)
        transaction.commit()
    }

    private fun provideFragmentForTag(tag: String, bundle: Bundle? = null): Fragment? {
        return if (supportFragmentManager.findFragmentByTag(tag) != null) {
            supportFragmentManager.findFragmentByTag(tag)
        } else {
            when (tag) {
                MovieFragment.TAG -> MovieFragment.newInstance()
                DetailsFragment.TAG -> bundle?.let { DetailsFragment.newInstance(it) }
                ErrorFragment.TAG -> ErrorFragment.newInstance()
                else -> Fragment()
            }
        }
    }

    override fun onRetryPressed() {
        if (NetworkState.isConnectedToNetwork(this)) {
            showMovieScreen()
        }
    }

    override fun onBackPressed() {
        if (getCurrentFragment()?.tag == DetailsFragment.TAG) {
            supportFragmentManager.popBackStackImmediate()
            return
        }
        if (supportFragmentManager.backStackEntryCount > 0) {
            super.onBackPressed()
        }
    }

    private fun getCurrentFragment(): Fragment? {
        return if (supportFragmentManager.fragments.size > 0) {
            supportFragmentManager.fragments[supportFragmentManager.fragments.size - 1]
        } else {
            null
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putString(LAST_FRAGMENT_NAME_KEY, getCurrentFragment()?.tag)
        super.onSaveInstanceState(outState)
    }

    private fun showErrorScreen() {
        supportFragmentManager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
        showFragmentWithTag(ErrorFragment.TAG)
    }

    private fun showDetailsScreen(item: MovieInfo) {
        val bundle = Bundle()
        bundle.putParcelable(DetailsFragment.SELECTED_CHARACTER_KEY, item)
        showFragmentWithTag(DetailsFragment.TAG, bundle)
    }

    private fun showMovieScreen() {
        showFragmentWithTag(MovieFragment.TAG)
    }

    override fun onItemClicked(item: MovieInfo) {
        showDetailsScreen(item)
    }

    override fun onError() {
        showErrorScreen()
    }
}