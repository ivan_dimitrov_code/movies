package com.puzzlegramma.movie.api

import com.puzzlegramma.movie.screens.movie.model.server.MovieServerModel
import io.reactivex.Single
import retrofit2.http.GET

interface ApiInterface {
    @GET("FEND16/movie-json-data/master/json/movies-coming-soon.json")
    fun requestListData(): Single<List<MovieServerModel>>
}