package com.puzzlegramma.movie.utils

import android.widget.ImageView
import com.puzzlegramma.movie.R
import com.squareup.picasso.Picasso

fun loadImage(uri: String, placeHolder: ImageView) {
    if (uri.isEmpty()) {
        Picasso.get()
            .load(R.drawable.ic_baseline_image_not_supported_24)
            .error(R.drawable.ic_baseline_image_not_supported_24)
            .into(placeHolder)
    } else {
        Picasso.get()
            .load(uri)
            .error(R.drawable.ic_baseline_image_not_supported_24)
            .into(placeHolder)
    }
}