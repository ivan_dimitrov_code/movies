package com.puzzlegramma.movie.screens.movie.ui

import android.content.Context
import android.os.Bundle
import android.view.*
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.puzzlegramma.movie.R
import com.puzzlegramma.movie.screens.movie.MovieViewModel
import com.puzzlegramma.movie.screens.movie.model.app.MovieInfo
import kotlinx.android.synthetic.main.fragment_movies.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class MovieFragment : Fragment(), MovieAdapter.OnMovieListInteractionListener {
    private val viewModel by viewModel<MovieViewModel>()
    private val adapter: MovieAdapter = MovieAdapter(mutableListOf(), this)
    private var listener: OnFragmentInteractionListener? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_movies, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        movieListRecyclerView.addItemDecoration(
            DividerItemDecoration(
                movieListRecyclerView.context,
                DividerItemDecoration.VERTICAL
            )
        )

        movieListRecyclerView.layoutManager = LinearLayoutManager(context)
        movieListRecyclerView.adapter = adapter
        startLiveDataObservation()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        requireActivity().menuInflater.inflate(R.menu.main_menu, menu)
        val menuItem: MenuItem = menu.findItem(R.id.action_search)
        val searchView: SearchView = menuItem.actionView as SearchView
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                adapter.filter.filter(newText)
                return false
            }
        })
        super.onCreateOptionsMenu(menu, inflater)
    }

    private fun startLiveDataObservation() {
        viewModel.uiState.observe(viewLifecycleOwner, {
            it?.let { state ->
                when (state) {
                    is MovieState.DefaultState -> enterLoadedItemsState(state.movieList)
                    is MovieState.LoadingState -> enterLoadingState()
                    is MovieState.ErrorState -> listener?.onError()
                }
            }
        })
        viewModel.requestMovieData()
    }

    private fun enterLoadingState() {
        movieListRecyclerView.visibility = View.INVISIBLE
        movieListProgressBar.visibility = View.VISIBLE
    }

    private fun enterLoadedItemsState(list: List<MovieInfo>) {
        adapter.injectList(list)
        movieListRecyclerView.visibility = View.VISIBLE
        movieListProgressBar.visibility = View.INVISIBLE
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnFragmentInteractionListener")
        }
    }

    interface OnFragmentInteractionListener {
        fun onItemClicked(item: MovieInfo)
        fun onError()
    }

    companion object {
        const val TAG = "MovieFragment"
        fun newInstance() = MovieFragment()
    }

    override fun onMovieClick(characterModel: MovieInfo) {
        listener?.onItemClicked(characterModel)
    }
}