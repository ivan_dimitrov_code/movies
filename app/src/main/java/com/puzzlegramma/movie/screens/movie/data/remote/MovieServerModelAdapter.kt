package com.puzzlegramma.movie.screens.movie.data.remote

import com.puzzlegramma.movie.screens.movie.model.app.MovieInfo
import com.puzzlegramma.movie.screens.movie.model.server.MovieServerModel
import kotlin.math.floor

fun adaptMovieServerModelToAppModel(serverModelList: List<MovieServerModel>): List<MovieInfo> {
    return serverModelList.map {
        val averageRating = floor(it.ratings.average() * 100) / 100
        MovieInfo(
            it.id,
            it.title,
            it.year,
            it.genres,
            it.ratings,
            it.poster,
            it.contentRating,
            it.duration,
            it.releaseDate,
            averageRating,
            it.originalTitle,
            it.storyline,
            it.actors,
            it.imdbRating,
            it.posterurl
        )
    }
}