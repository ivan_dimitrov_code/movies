package com.puzzlegramma.movie.screens.movie.data

import com.puzzlegramma.movie.screens.movie.model.app.MovieInfo
import io.reactivex.Single

open class MovieRepository(private val remoteDataSource: MovieDataSource) {

    open fun provideMovieData(): Single<List<MovieInfo>> {
        return remoteDataSource.provideMovieData()
    }
}
