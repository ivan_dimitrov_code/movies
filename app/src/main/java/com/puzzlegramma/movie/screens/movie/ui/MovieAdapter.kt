package com.puzzlegramma.movie.screens.movie.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.puzzlegramma.movie.R
import com.puzzlegramma.movie.screens.movie.model.app.MovieInfo
import com.puzzlegramma.movie.utils.loadImage
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.cell_movie_list.view.*
import org.koin.ext.isInt

class MovieAdapter(
    private val list: MutableList<MovieInfo>,
    private val listener: OnMovieListInteractionListener
) : RecyclerView.Adapter<MovieAdapter.ViewHolder>(), Filterable {

    private val displayedList: MutableList<MovieInfo> = mutableListOf()

    fun injectList(newList: List<MovieInfo>) {
        list.clear()
        list.addAll(newList)
        displayedList.clear()
        displayedList.addAll(newList)
        notifyDataSetChanged()
    }

    interface OnMovieListInteractionListener {
        fun onMovieClick(characterModel: MovieInfo)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.cell_movie_list, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = displayedList[position]
        holder.view.setOnClickListener {
            listener.onMovieClick(item)
        }
        loadImage(item.posterUrl, holder.image)
        holder.name.text = item.title
    }

    override fun getItemCount(): Int = displayedList.size

    inner class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val name: TextView = view.movieName
        val image: ImageView = view.movieImage
    }

    override fun getFilter(): Filter {
        return characterFilter
    }

    var characterFilter: Filter = object : Filter() {
        override fun performFiltering(charSequence: CharSequence): FilterResults {
            val filteredList: MutableList<MovieInfo> = ArrayList()

            if (charSequence.isEmpty()) {
                filteredList.addAll(list)
            } else {
                for (character in list) {
                    if ((charSequence.toString().isInt())
                        || filterForName(character, charSequence.toString())
                    ) {
                        filteredList.add(character)
                    }
                }
            }
            val filterResults = FilterResults()
            filterResults.values = filteredList
            return filterResults
        }

        fun filterForName(characterModel: MovieInfo, name: String): Boolean {
            return characterModel.title.toLowerCase()
                .contains(name.toLowerCase())
        }

        override fun publishResults(
            charSequence: CharSequence,
            filterResults: FilterResults
        ) {
            displayedList.clear()
            displayedList.addAll(filterResults.values as Collection<MovieInfo>)
            notifyDataSetChanged()
        }
    }
}
