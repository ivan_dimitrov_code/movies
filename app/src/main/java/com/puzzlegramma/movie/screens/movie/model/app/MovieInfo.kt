package com.puzzlegramma.movie.screens.movie.model.app

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MovieInfo(
    val id: String,
    val title: String,
    val year: String,
    val genresList: List<String>,
    val ratings: List<Int>,
    val poster: String,
    val contentRating: String,
    val duration: String,
    val releaseDate: String,
    val averageRating: Double,
    val originalTitle: String,
    val description: String,
    val actorsList: List<String>,
    val imdbRating: String,
    val posterUrl: String,
) : Parcelable