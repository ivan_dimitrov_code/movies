package com.puzzlegramma.movie.screens.details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.puzzlegramma.movie.R
import com.puzzlegramma.movie.screens.movie.model.app.MovieInfo
import com.puzzlegramma.movie.utils.loadImage
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_details.*

class DetailsFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_details, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val movieInfo =
            arguments?.getParcelable<MovieInfo>(SELECTED_CHARACTER_KEY) as MovieInfo

        loadImage(movieInfo.posterUrl, detailsImage)
        detailsTitle.text = movieInfo.title
        detailsYear.text = movieInfo.year
        detailsRating.text = movieInfo.averageRating.toString()
        detailsDuration.text = movieInfo.duration
        detailsGanres.text = movieInfo.genresList.toString()
        detailsReleaseDate.text = movieInfo.releaseDate
        detailsDescription.text = movieInfo.description
    }

    companion object {
        const val TAG = "DetailsFragment"
        const val SELECTED_CHARACTER_KEY = "SelectedCharacterKey"

        @JvmStatic
        fun newInstance(bundle: Bundle) =
            DetailsFragment().apply {
                arguments = bundle
            }
    }

}