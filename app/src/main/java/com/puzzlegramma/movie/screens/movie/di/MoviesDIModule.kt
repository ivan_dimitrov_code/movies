package com.puzzlegramma.movie.screens.movie.di

import com.puzzlegramma.movie.api.ApiClient
import com.puzzlegramma.movie.api.ApiInterface
import com.puzzlegramma.movie.screens.movie.MovieViewModel
import com.puzzlegramma.movie.screens.movie.data.MovieDataSource
import com.puzzlegramma.movie.screens.movie.data.MovieRepository
import com.puzzlegramma.movie.screens.movie.data.remote.MovieRemoteDataSource
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val movieModule = module {
    viewModel {
        MovieViewModel(get())
    }

    fun provideRepository(remoteDataSource: MovieDataSource): MovieRepository {
        return MovieRepository(remoteDataSource)
    }
    single { provideRepository(get()) }

    fun provideChuckNorrisDataSourceModule(api: ApiInterface): MovieDataSource {
        return MovieRemoteDataSource(api)
    }
    single { provideChuckNorrisDataSourceModule(get()) }

    single { ApiClient.getClient() }

}
