package com.puzzlegramma.movie.screens.movie

import android.annotation.SuppressLint
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.ViewModel
import com.puzzlegramma.movie.screens.movie.data.MovieRepository
import com.puzzlegramma.movie.screens.movie.ui.MovieState
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

open class MovieViewModel constructor(
    private val movieRepository: MovieRepository,
) : ViewModel() {
    val uiState = MutableLiveData<MovieState>()
    private val disposable = CompositeDisposable()

    open fun requestMovieData() {
        uiState.value = MovieState.LoadingState
        val requestDisposable = movieRepository.provideMovieData()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe({
                uiState.value = MovieState.DefaultState(it)
            }, {
                uiState.value = it.message?.let { errorMessage ->
                    MovieState.ErrorState(errorMessage)
                }
            })
        disposable.add(requestDisposable)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun clearDisposable() {
        disposable.clear()
    }
}
