package com.puzzlegramma.movie.screens.movie.data.remote

import com.puzzlegramma.movie.api.ApiInterface
import com.puzzlegramma.movie.screens.movie.data.MovieDataSource
import com.puzzlegramma.movie.screens.movie.model.app.MovieInfo
import io.reactivex.Single

class MovieRemoteDataSource(private val apiClient: ApiInterface) : MovieDataSource {
    override fun provideMovieData(): Single<List<MovieInfo>> {
        return apiClient.requestListData().map {
            adaptMovieServerModelToAppModel(it)
        }
    }
}
