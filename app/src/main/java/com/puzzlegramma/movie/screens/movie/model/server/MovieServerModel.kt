package com.puzzlegramma.movie.screens.movie.model.server

import com.google.gson.annotations.SerializedName

data class MovieServerModel(
    @SerializedName("id") val id: String,
    @SerializedName("title") val title: String,
    @SerializedName("year") val year: String,
    @SerializedName("genres") val genres: List<String>,
    @SerializedName("ratings") val ratings: List<Int>,
    @SerializedName("poster") val poster: String,
    @SerializedName("contentRating") val contentRating: String,
    @SerializedName("duration") val duration: String,
    @SerializedName("releaseDate") val releaseDate: String,
    @SerializedName("averageRating") val averageRating: Int,
    @SerializedName("originalTitle") val originalTitle: String,
    @SerializedName("storyline") val storyline: String,
    @SerializedName("actors") val actors: List<String>,
    @SerializedName("imdbRating") val imdbRating: String,
    @SerializedName("posterurl") val posterurl: String,
)