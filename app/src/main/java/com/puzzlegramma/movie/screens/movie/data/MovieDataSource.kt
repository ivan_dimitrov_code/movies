package com.puzzlegramma.movie.screens.movie.data

import com.puzzlegramma.movie.screens.movie.model.app.MovieInfo
import io.reactivex.Single

interface MovieDataSource {
    fun provideMovieData(): Single<List<MovieInfo>>
}
