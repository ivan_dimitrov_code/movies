package com.puzzlegramma.movie.screens.movie.ui

import com.puzzlegramma.movie.screens.movie.model.app.MovieInfo

sealed class MovieState {
    object LoadingState : MovieState()
    data class DefaultState(val movieList: List<MovieInfo>) : MovieState()
    data class ErrorState(val errorMessage: String) : MovieState()
}


