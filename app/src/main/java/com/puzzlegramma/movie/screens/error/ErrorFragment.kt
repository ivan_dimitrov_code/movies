package com.puzzlegramma.movie.screens.error

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.puzzlegramma.movie.R
import kotlinx.android.synthetic.main.fragment_error.*

class ErrorFragment : Fragment() {

    private lateinit var listener: OneErrorFragmentInteractionListener

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_error, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        button.setOnClickListener {
            listener.onRetryPressed()
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OneErrorFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OneErrorFragmentInteractionListener")
        }
    }

    companion object {
        const val TAG = "ErrorFragment"
        fun newInstance() = ErrorFragment()
    }

    interface OneErrorFragmentInteractionListener {
        fun onRetryPressed()
    }
}